#include "necstream.hpp"
//#include <hls_stream.h>
#include <stdint.h>
#include <ap_int.h>
//#include <iostream>

#define ONE 1.0f

typedef ap_uint<1> data_bool;

FloatAxis f_2_s(float data, ap_uint<1> last)
{
	FloatAxis retValue;
	memsetAXIS_Data<FloatAxis>(retValue, 32);
	retValue.data = data;
	retValue.last = last;
	return retValue;
}

void RRSS_gen_full(hls::stream<float> &src_axi, hls::stream<FloatAxis> &RR_axi, hls::stream<FloatAxis> &SD_axi)
{
#pragma HLS INTERFACE axis register both port=src_axi
#pragma HLS INTERFACE axis register both port=RR_axi
#pragma HLS INTERFACE axis register both port=SD_axi
#pragma HLS INTERFACE ap_ctrl_none port=return

	float tempRR[4] = {0.0, 0.0,0.0,0.0};
#pragma HLS RESOURCE variable=tempRR core=RAM_2P_BRAM
//#pragma HLS ARRAY_PARTITION variable=tempRR complete dim=0
//#pragma HLS RESET variable=tempRR

	static float outRR[3] = {0.0, 0.0,0.0};
#pragma HLS RESOURCE variable=outRR core=RAM_2P_BRAM
//#pragma HLS ARRAY_PARTITION variable=outRR complete dim=0
	float outSSD = 0.0;
#pragma HLS RESOURCE variable=outSSD core=RAM_2P_BRAM
//#pragma HLS ARRAY_PARTITION variable=outSSD complete dim=0
	float tempResult;
	data_bool buffer_count = 0;

	static float sampleCounter = 0.0;
#pragma HLS RESET variable=sampleCounter
	int count;
	int i,j,k;

	static float RRCoeff = 0.0;
	static float SDCoeff = 0.0;

	float input;

	// Read and push number of samples
	//read_sample_count(src_axi, &sampleCounter, &count);
	input = src_axi.read();//streamPop<float, FloatAxis, FloatStream>(src_axi);
	sampleCounter = input;
	count = (int)sampleCounter;
	RR_axi.write(f_2_s(sampleCounter, 0));//streamPush<float, FloatAxis, FloatStream>(sampleCounter, 0, RR_axi, 32);
	SD_axi.write(f_2_s(sampleCounter-1.0, 0));//streamPush<float, FloatAxis, FloatStream>(sampleCounter, 0, SD_axi, 32);

	// Push coefficients
	coeff_push:
	{
#pragma HLS UNROLL
		input = src_axi.read();//streamPop<float, FloatAxis, FloatStream>(src_axi);
		RRCoeff = input;
		RR_axi.write(f_2_s(RRCoeff, 0));//streamPush<float, FloatAxis, FloatStream>(RRCoeff, 0, RR_axi, 32);
		input = src_axi.read();//streamPop<float, FloatAxis, FloatStream>(src_axi);
		SDCoeff = input;
		SD_axi.write(f_2_s(SDCoeff, 0));//streamPush<float, FloatAxis, FloatStream>(SDCoeff, 0, SD_axi, 32);
	}

	// First input
	first_input:
	{
#pragma HLS UNROLL
		tempRR[0] = src_axi.read();
		tempRR[1] = src_axi.read();
		tempRR[2] = src_axi.read();
		outRR[0] = tempRR[1]-tempRR[0];
		outRR[1] = tempRR[2]-tempRR[1];
		RR_axi.write(f_2_s(outRR[0], 0));
		RR_axi.write(f_2_s(outRR[1], 0));
	}

	// Read stream
	read_loop: for(i=2; i<count; i++)
	{
//#pragma HLS UNROLL
#pragma HLS PIPELINE II=1
#pragma HLS LOOP_TRIPCOUNT min=2 max=6 avg=4
		tempRR[3] = src_axi.read();//streamPop<float, FloatAxis, FloatStream>(src_axi);
		outRR[2] = tempRR[3] - tempRR[2];
		outSSD = outRR[1]-outRR[0];
		//outRR[2] = tempResult;
		RR_axi.write(f_2_s(outRR[2], i==(count-1)));//streamPush<float, FloatAxis, FloatStream>(outRR[buffer_count], i==(count-1), RR_axi, 32);
		SD_axi.write(f_2_s(outSSD, 0));//streamPush<float, FloatAxis, FloatStream>(outRR[buffer_count], i==(count-1), SD_axi, 32);
		shift_reg_RR: for(j=0; j<3; j++)
		{
#pragma HLS UNROLL
			tempRR[j] = tempRR[j+1];
		}
		shift_reg_SSD: for(k=0; k<2; k++)
		{
#pragma HLS UNROLL
			outRR[k] = outRR[k+1];
		}

	}

	// Last SSD
	outSSD = outRR[1]-outRR[0];
	SD_axi.write(f_2_s(outSSD, 1));
}
