#include "necstream.hpp"
//#include "ap_axi_sdata.h"
//#include <hls_stream.h>
#include<stdint.h>
//#include <hls_video.h>
#include <iostream>
#include <stdio.h>

#define DATA_LEN 10

typedef struct{
	float data;
	ap_uint<1> last;
} sFloat;


//void RR_gen(FloatStream &src_axi, FloatStream &RR_axi, FloatStream &SD_axi);
void RRSS_gen_full(hls::stream<float> &src_axi, hls::stream<FloatAxis> &RR_axi, hls::stream<FloatAxis> &SD_axi);

int main() {
	hls::stream<float> in("stream_in");
	hls::stream<FloatAxis> outRR("stream_RR_out");
	hls::stream<FloatAxis> outSD("stream_SD_out");
	//FloatStream fwd("stream_fwd");

	float sampleCount = (float)(DATA_LEN-1.0);
	float test_data[DATA_LEN] = {120.0658046572696,120.6829186916535,121.3038984873769,
			121.8928126855711,122.5097542367874,123.151374829189,123.7606772935173,
			124.3660339491297,124.9462828947557,125.563224445972};
	FloatAxis resultRR;
	FloatAxis resultSD;
	int i=0;

	// Stream support data
	in.write(sampleCount);//streamPush<float, FloatAxis, FloatStream>(sampleCount, 0, in, 32);
	in.write(1.0/(float)(DATA_LEN-1.0));//streamPush<float, FloatAxis, FloatStream>(1.0/(float)(DATA_LEN-1.0), 0, in, 32);
	in.write(1.0/(float)(DATA_LEN-2.0));//streamPush<float, FloatAxis, FloatStream>(1.0/(float)(DATA_LEN-2.0), 0, in, 32);

	// Stream real data
	for(i=0; i<DATA_LEN; i++)
	{
		in.write(test_data[i]);//streamPush<float, FloatAxis, FloatStream>(test_data[i], i==(DATA_LEN-1), in, 32);
	}

	RRSS_gen_full(in, outRR, outSD);

	// Get number of samples coefficients
	resultRR = outRR.read();//streamPop<float, FloatAxis, FloatStream>(outRR);
	resultSD = outSD.read();//streamPop<float, FloatAxis, FloatStream>(outSD);
	std::cout << "RR samples " << resultRR.data << " SD samples " << resultSD.data << std::endl;
	resultRR = outRR.read(); //streamPop<float, FloatAxis, FloatStream>(outRR);
	resultSD = outSD.read();//streamPop<float, FloatAxis, FloatStream>(outSD);
	std::cout << "RR coeff " << resultRR.data << " SD coeff " << resultSD.data << std::endl;

	for(i=0; i<DATA_LEN-1; i++)
	{
		resultRR = outRR.read(); //streamPop<float, FloatAxis, FloatStream>(outRR);
		if(i<DATA_LEN-2)
			resultSD = outSD.read();//streamPop<float, FloatAxis, FloatStream>(outSD);
		std::cout << "RR data " << resultRR.data << " " << resultRR.last << " SD data " << resultSD.data << " " << resultSD.last << std::endl;
	}
	return 0;
}
