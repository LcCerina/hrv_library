#include "necstream.hpp"
//#include <hls_stream.h>
#include <stdint.h>
#include <ap_int.h>
//#include <iostream>

#define ONE 1.0f

typedef ap_uint<1> data_bool;

typedef struct{
	float data;
	ap_uint<1> last;
} sFloat;

FloatAxis f_2_s(float data, ap_uint<1> last)
{
	FloatAxis retValue;
	memsetAXIS_Data<FloatAxis>(retValue, 32);
	retValue.data = data;
	retValue.last = last;
	return retValue;
}

void RR_gen_full(hls::stream<float> &src_axi, hls::stream<FloatAxis> &RR_axi, hls::stream<FloatAxis> &SD_axi)
{
#pragma HLS INTERFACE axis register both port=src_axi
#pragma HLS INTERFACE axis register both port=RR_axi
#pragma HLS INTERFACE axis register both port=SD_axi
#pragma HLS INTERFACE ap_ctrl_none port=return

	float tempRR[2] = {0.0, 1.0};
#pragma HLS ARRAY_PARTITION variable=tempRR complete dim=1
//#pragma HLS RESET variable=tempRR

	float outRR[2] = {0.0, 1.0};
#pragma HLS ARRAY_PARTITION variable=outRR complete dim=1
	data_bool buffer_count = 0;

	static float sampleCounter = 0.0;
#pragma HLS RESET variable=sampleCounter
	int count;
	int i;

	static float RRCoeff = 0.0;
	static float SDCoeff = 0.0;

	float input;

	// Read and push number of samples
	//read_sample_count(src_axi, &sampleCounter, &count);
	input = src_axi.read();//streamPop<float, FloatAxis, FloatStream>(src_axi);
	sampleCounter = input;
	count = (int)sampleCounter;
	RR_axi.write(f_2_s(sampleCounter, 0));//streamPush<float, FloatAxis, FloatStream>(sampleCounter, 0, RR_axi, 32);
	SD_axi.write(f_2_s(sampleCounter, 0));//streamPush<float, FloatAxis, FloatStream>(sampleCounter, 0, SD_axi, 32);

	// Push coefficients
	coeff_push:
	{
		input = src_axi.read();//streamPop<float, FloatAxis, FloatStream>(src_axi);
		RRCoeff = input;
		RR_axi.write(f_2_s(RRCoeff, 0));//streamPush<float, FloatAxis, FloatStream>(RRCoeff, 0, RR_axi, 32);
		input = src_axi.read();//streamPop<float, FloatAxis, FloatStream>(src_axi);
		SDCoeff = input;
		SD_axi.write(f_2_s(SDCoeff, 0));//streamPush<float, FloatAxis, FloatStream>(SDCoeff, 0, SD_axi, 32);
	}

	// First input
	tempRR[0] = src_axi.read();//streamPop<float, FloatAxis, FloatStream>(src_axi);

	// Read stream
	read_loop: for(i=0; i<count; i++)
	{
#pragma HLS PIPELINE II=1
#pragma HLS LOOP_TRIPCOUNT min=2 max=6 avg=4
		tempRR[1] = src_axi.read();//streamPop<float, FloatAxis, FloatStream>(src_axi);
		outRR[buffer_count] = tempRR[1] - tempRR[0];
		RR_axi.write(f_2_s(outRR[buffer_count], i==(count-1)));//streamPush<float, FloatAxis, FloatStream>(outRR[buffer_count], i==(count-1), RR_axi, 32);
		SD_axi.write(f_2_s(outRR[buffer_count], i==(count-1)));//streamPush<float, FloatAxis, FloatStream>(outRR[buffer_count], i==(count-1), SD_axi, 32);
		++buffer_count;
		tempRR[0] = tempRR[1];
	}
}
