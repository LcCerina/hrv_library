#include "necstream.hpp"
//#include "ap_axi_sdata.h"
//#include <hls_stream.h>
#include<stdint.h>
//#include <hls_video.h>
#include <iostream>
#include <stdio.h>

#define DATA_LEN 7

typedef struct{
	float data;
	ap_uint<1> last;
} sFloat;


//void RR_gen(FloatStream &src_axi, FloatStream &RR_axi, FloatStream &SD_axi);
void RR_gen_full(hls::stream<float> &src_axi, hls::stream<FloatAxis> &RR_axi, hls::stream<FloatAxis> &SD_axi);

int main() {
	hls::stream<float> in("stream_in");
	hls::stream<FloatAxis> outRR("stream_RR_out");
	hls::stream<FloatAxis> outSD("stream_SD_out");
	//FloatStream fwd("stream_fwd");

	float sampleCount = (float)(DATA_LEN-1.0);
	float test_data[DATA_LEN] = {1.0, 2.02, 2.976, 4.088, 4.932, 5.916, 6.968};
	FloatAxis resultRR;
	FloatAxis resultSD;
	int i=0;

	// Stream support data
	in.write(sampleCount);//streamPush<float, FloatAxis, FloatStream>(sampleCount, 0, in, 32);
	in.write(1.0/(float)(DATA_LEN-1.0));//streamPush<float, FloatAxis, FloatStream>(1.0/(float)(DATA_LEN-1.0), 0, in, 32);
	in.write(1.0/(float)(DATA_LEN-2.0));//streamPush<float, FloatAxis, FloatStream>(1.0/(float)(DATA_LEN-2.0), 0, in, 32);

	// Stream real data
	for(i=0; i<DATA_LEN; i++)
	{
		in.write(test_data[i]);//streamPush<float, FloatAxis, FloatStream>(test_data[i], i==(DATA_LEN-1), in, 32);
	}

	RR_gen_full(in, outRR, outSD);

	// Get number of samples coefficients
	resultRR = outRR.read();//streamPop<float, FloatAxis, FloatStream>(outRR);
	resultSD = outSD.read();//streamPop<float, FloatAxis, FloatStream>(outSD);
	std::cout << "RR samples " << resultRR.last << " SD samples " << resultSD.last << std::endl;
	resultRR = outRR.read(); //streamPop<float, FloatAxis, FloatStream>(outRR);
	resultSD = outSD.read();//streamPop<float, FloatAxis, FloatStream>(outSD);
	std::cout << "RR coeff " << resultRR.last << " SD coeff " << resultSD.last << std::endl;

	for(i=0; i<DATA_LEN-1; i++)
	{
		resultRR = outRR.read(); //streamPop<float, FloatAxis, FloatStream>(outRR);
		resultSD = outSD.read();//streamPop<float, FloatAxis, FloatStream>(outSD);
		std::cout << "RR data " << resultRR.last << " SD data " << resultSD.last << std::endl;
	}
	return 0;
}
