#include "necstream.hpp"
#include <hls_stream.h>
#include <stdint.h>
#include <ap_int.h>

#define ONE 1.0f

typedef ap_uint<1> data_bool;

//typedef hls::stream<float> FloatStream;

void RR_gen(FloatStream &src_axi, FloatStream &RR_axi, FloatStream &SD_axi)
{
#pragma HLS INTERFACE axis port=RR_axi
#pragma HLS INTERFACE axis port=src_axi
#pragma HLS INTERFACE axis port=SD_axi
#pragma HLS INTERFACE ap_ctrl_none port=return

	static float tempRR[2] = {0.0, 0.0};
#pragma HLS ARRAY_PARTITION variable=tempRR complete dim=1
#pragma HLS RESET variable=tempRR
	float outRR;

	static data_bool initFlag = 0;
#pragma HLS RESET variable=initFlag

	static int sampleCounter = 0;
#pragma HLS RESET variable=sampleCounter

	static float RRCoeff = 0.0;

	FloatAxis input;

	// Input
	input = streamPopFull<FloatAxis, FloatStream>(src_axi);

	// Calculate RR and SD
	tempRR[1] = input.data;

	// perform calculation
	if(initFlag)
	{
		++sampleCounter;
		outRR = tempRR[1] - tempRR[0];
		streamPush<float, FloatAxis, FloatStream>(outRR, 0, RR_axi, 32);
		streamPush<float, FloatAxis, FloatStream>(outRR, input.last==1, SD_axi, 32); //TODO fix this
	}
	else
	{
		initFlag = 1;
	}

	// Shift values
	tempRR[0] = tempRR[1];

	// Check input
	if(input.last)
	{
		// Push out also 1.0 / samples for downstream cores
		RRCoeff = ONE / (float)sampleCounter;
		streamPush<float, FloatAxis, FloatStream>(RRCoeff, 1, RR_axi, 32);
	}
}
