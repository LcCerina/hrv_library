#include "necstream.hpp"
//#include "ap_axi_sdata.h"
#include <hls_stream.h>
#include<stdint.h>
//#include <hls_video.h>
#include <iostream>
#include <stdio.h>

#define DATA_LEN 7

//typedef hls::stream<float> FloatStream;

void RR_gen(FloatStream &src_axi, FloatStream &RR_axi, FloatStream &SD_axi);

int main() {
	FloatStream in("stream_in");
	FloatStream outRR("stream_RR_out");
	FloatStream outSD("stream_SD_out");
	//FloatStream fwd("stream_fwd");

	float test_data[DATA_LEN] = {1.0, 2.02, 2.976, 4.088, 4.932, 5.916, 6.968};
	float resultRR;
	float resultSD;

	for(int i=0; i<DATA_LEN; i++)
	{
		streamPush<float, FloatAxis, FloatStream>(test_data[i], i==DATA_LEN-1, in, 32);
		RR_gen(in, outRR, outSD);
		if(i>=1)
		{
			resultRR = streamPop<float, FloatAxis, FloatStream>(outRR);
			resultSD = streamPop<float, FloatAxis, FloatStream>(outSD);
			std::cout << "RR " << resultRR << " SD " << resultSD << std::endl;
		}
	}
	// Get number of samples coefficients
	resultRR = streamPop<float, FloatAxis, FloatStream>(outRR);
	std::cout << "RR coeff " << resultRR << " SD coeff " << resultSD << std::endl;
	return 0;
}
j
