#include "necstream.hpp"
//#include <hls_stream.h>
#include <stdint.h>
#include <ap_int.h>
//#include <iostream>

#define ONE 1.0f

typedef ap_uint<1> data_bool;

FloatAxis f_2_s(float data, ap_uint<1> last)
{
	FloatAxis retValue;
	memsetAXIS_Data<FloatAxis>(retValue, 32);
	retValue.data = data;
	retValue.last = last;
	return retValue;
}

void SD_gen_full(hls::stream<FloatAxis> &RR_axi, hls::stream<FloatAxis> &SD_axi)
{
#pragma HLS INTERFACE axis register both port=RR_axi
#pragma HLS INTERFACE axis register both port=SD_axi
#pragma HLS INTERFACE ap_ctrl_none port=return

	float tempSD[2] = {0.0, 1.0};
#pragma HLS ARRAY_PARTITION variable=tempSD complete dim=1
//#pragma HLS RESET variable=tempRR

	float outSD[2] = {0.0, 1.0};
#pragma HLS ARRAY_PARTITION variable=outSD complete dim=1
	data_bool buffer_count = 0;

	static float sampleCounter = 0.0;
#pragma HLS RESET variable=sampleCounter
	int count;
	int i;

	static float SDCoeff = 0.0;

	FloatAxis input;

	// Read and push number of samples
	//read_sample_count(src_axi, &sampleCounter, &count);
	input = RR_axi.read();//streamPop<float, FloatAxis, FloatStream>(src_axi);
	sampleCounter = input.data;
	count = (int)sampleCounter-1;
	SD_axi.write(f_2_s(sampleCounter-1.0, 0));//streamPush<float, FloatAxis, FloatStream>(sampleCounter, 0, SD_axi, 32);

	// Push coefficients
	coeff_push:
	{
		input = RR_axi.read();//streamPop<float, FloatAxis, FloatStream>(src_axi);
		SDCoeff = input.data;
		SD_axi.write(f_2_s(SDCoeff, 0));//streamPush<float, FloatAxis, FloatStream>(RRCoeff, 0, RR_axi, 32);
	}

	// First input
	tempSD[0] = RR_axi.read().data;//streamPop<float, FloatAxis, FloatStream>(src_axi);

	// Read stream
	read_loop: for(i=0; i<count; i++)
	{
#pragma HLS PIPELINE II=1
#pragma HLS LOOP_TRIPCOUNT min=2 max=6 avg=4
		tempSD[1] = RR_axi.read().data;//streamPop<float, FloatAxis, FloatStream>(src_axi);
		outSD[buffer_count] = tempSD[1] - tempSD[0];
		SD_axi.write(f_2_s(outSD[buffer_count], i==(count-1)));//streamPush<float, FloatAxis, FloatStream>(outRR[buffer_count], i==(count-1), SD_axi, 32);
		++buffer_count;
		tempSD[0] = tempSD[1];
	}
}
