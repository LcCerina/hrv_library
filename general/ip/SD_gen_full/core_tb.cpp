#include "necstream.hpp"
//#include "ap_axi_sdata.h"
//#include <hls_stream.h>
#include<stdint.h>
//#include <hls_video.h>
#include <iostream>
#include <stdio.h>

#define DATA_LEN 6

//void RR_gen(FloatStream &src_axi, FloatStream &RR_axi, FloatStream &SD_axi);
void SD_gen_full(hls::stream<FloatAxis> &RR_axi, hls::stream<FloatAxis> &SD_axi);
FloatAxis f_2_s(float data, ap_uint<1> last);

int main() {
	hls::stream<FloatAxis> in("stream_in");
	hls::stream<FloatAxis> outSD("stream_SD_out");
	//FloatStream fwd("stream_fwd");

	float sampleCount = (float)(DATA_LEN);
	float test_data[DATA_LEN] = {1.02 , 0.956, 1.112, 0.844, 0.984, 1.052};
	FloatAxis resultSD;
	int i=0;

	// Stream support data
	in.write(f_2_s(sampleCount, 0));//streamPush<float, FloatAxis, FloatStream>(sampleCount, 0, in, 32);
	in.write(f_2_s(1.0/(float)(DATA_LEN-1.0), 0));//streamPush<float, FloatAxis, FloatStream>(1.0/(float)(DATA_LEN-1.0), 0, in, 32);

	// Stream real data
	for(i=0; i<DATA_LEN; i++)
	{
		in.write(f_2_s(test_data[i], i==DATA_LEN-1));//streamPush<float, FloatAxis, FloatStream>(test_data[i], i==(DATA_LEN-1), in, 32);
	}

	SD_gen_full(in, outSD);

	// Get number of samples coefficients

	resultSD = outSD.read();//streamPop<float, FloatAxis, FloatStream>(outSD);
	std::cout << "SD samples " << resultSD.data << std::endl;
	resultSD = outSD.read();//streamPop<float, FloatAxis, FloatStream>(outSD);
	std::cout << "SD coeff " << resultSD.data << std::endl;

	for(i=0; i<DATA_LEN-1; i++)
	{

		resultSD = outSD.read();//streamPop<float, FloatAxis, FloatStream>(outSD);
		std::cout << "SD data " << resultSD.data << std::endl;
	}
	return 0;
}
