#include "necstream.hpp"
#include <hls_stream.h>
#include <stdint.h>
#include <ap_int.h>

#define ONE 1.0f

typedef ap_uint<1> data_bool;

void SD_gen(FloatStream &src_axi, FloatStream &SD_axi)
{
#pragma HLS INTERFACE axis port=src_axi
#pragma HLS INTERFACE axis port=SD_axi
#pragma HLS INTERFACE ap_ctrl_none port=return

	static float tempSD[2] = {0.0, 0.0};
#pragma HLS ARRAY_PARTITION variable=tempSD complete dim=1
#pragma HLS RESET variable=tempSD
	float outSD;

	static data_bool initFlag = 0;
#pragma HLS RESET variable=initFlag

	static int sampleCounter = 0;
#pragma HLS RESET variable=sampleCounter

	static float SDCoeff = 0.0;

	FloatAxis input;

	// Input
	input = streamPopFull<FloatAxis, FloatStream>(src_axi);

	// Calculate RR and SD
	tempSD[1] = input.data;

	// perform calculation
	if(initFlag)
	{
		++sampleCounter;
		outSD = tempSD[1] - tempSD[0];
		streamPush<float, FloatAxis, FloatStream>(outSD, 0, SD_axi, 32);
	}
	else
	{
		initFlag = 1;
	}

	// Shift values
	tempSD[0] = tempSD[1];

	// Check input
	if(input.last)
	{
		// Push out also 1.0 / samples for downstream cores
		SDCoeff = ONE / (float)sampleCounter;
		streamPush<float, FloatAxis, FloatStream>(SDCoeff, 1, SD_axi, 32);
	}
}
