#include "necstream.hpp"
//#include "ap_axi_sdata.h"
#include <hls_stream.h>
#include<stdint.h>
//#include <hls_video.h>
#include <iostream>
#include <stdio.h>

#define DATA_LEN 6

//typedef hls::stream<float> FloatStream;

void SD_gen(FloatStream &src_axi, FloatStream &SD_axi);

int main() {
	FloatStream in("stream_in");
	FloatStream outSD("stream_SD_out");
	//FloatStream fwd("stream_fwd");

	float test_data[DATA_LEN] = {1.02, 0.956, 1.112, 0.844, 0.984, 1.052};
	float resultSD;

	for(int i=0; i<DATA_LEN; i++)
	{
		streamPush<float, FloatAxis, FloatStream>(test_data[i], i==DATA_LEN-1, in, 32);
		SD_gen(in, outSD);//, fwd);
		if(i>=1)
		{
			resultSD = streamPop<float, FloatAxis, FloatStream>(outSD);
			std::cout << "SD " << resultSD << std::endl;
		}
	}
	// Get number of samples coefficients
	resultSD = streamPop<float, FloatAxis, FloatStream>(outSD);
	std::cout << "SD coeff " << resultSD << std::endl;
	return 0;
}
