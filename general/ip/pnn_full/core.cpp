#include "necstream.hpp"
#include <hls_stream.h>
#include <stdint.h>
#include<stdio.h>

typedef ap_uint<1> data_bool;

FloatAxis f_2_s(float data, ap_uint<1> last)
{
	FloatAxis retValue;
	memsetAXIS_Data<FloatAxis>(retValue, 32);
	retValue.data = data;
	retValue.last = last;
	return retValue;
}

void pnn_full(FloatStream &rr_axi, FloatStream &pnn_axi)
{
//#pragma HLS DATAFLOW
#pragma HLS INTERFACE axis port=rr_axi
#pragma HLS INTERFACE axis port=pnn_axi
#pragma HLS INTERFACE ap_ctrl_none port=return
	static float threshold = 0.05;
	static int PnnCounter = 0;
#pragma HLS RESET variable=PnnCounter
	int tempCounter[2] = {0.0, 0.0};
#pragma HLS ARRAY_PARTITION variable=tempCounter complete dim=1

	float Pnn50;
	float mul_coeff;
	int count;
	int i;
	data_bool buffer_count = 0;
#pragma HLS RESET variable=buffer_count


	float rr_input[2] = {0.0, 0.0};
#pragma HLS ARRAY_PARTITION variable=rr_input complete dim=1


	FloatAxis input;

	// Input number of samples
	input = rr_axi.read();
	count = (int)input.data;

	//Input coefficients
	input = rr_axi.read();
	mul_coeff = input.data;

	// Read stream
	read_loop: for(i=0; i<count; i++)
	{
#pragma HLS PIPELINE II=1
#pragma HLS LOOP_TRIPCOUNT min=2 max=6 avg=4
		rr_input[buffer_count] = rr_axi.read().data;
		tempCounter[buffer_count] = hls::fabsf(rr_input[buffer_count]) > threshold;
		PnnCounter += tempCounter[buffer_count];
		++buffer_count;
	}

	// Output result
	Pnn50 = mul_coeff*PnnCounter;
	pnn_axi.write(f_2_s(Pnn50, 1));
}
