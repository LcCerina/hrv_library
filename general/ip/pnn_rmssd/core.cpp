#include "necstream.hpp"
#include <hls_stream.h>
#include <stdint.h>
#include<stdio.h>

//typedef hls::stream<float> FloatStream;

void pnn_rmssd(FloatStream &src_axi, FloatStream &dst_axi)//, FloatStream &fwd_axi)
{
//#pragma HLS DATAFLOW
#pragma HLS INTERFACE axis port=dst_axi
#pragma HLS INTERFACE axis port=src_axi
#pragma HLS INTERFACE ap_ctrl_none port=return
	static float threshold = 0.05;
	//static float tempSumSsd = 0.0;
//#pragma HLS RESET variable=tempSumSsd
	static int PnnCounter = 0;
#pragma HLS RESET variable=PnnCounter
	float Pnn50;
	//float SSD;

	FloatAxis input;

	// Input
	input = streamPopFull<FloatAxis, FloatStream>(src_axi);

	// Output result
	if(input.last)
	{
		// Get multiplication coefficient from stream
		Pnn50 = input.data*PnnCounter;
		//SSD = input.data*tempSumSsd;

		// Output values
	    streamPush<float, FloatAxis, FloatStream>(Pnn50, 1, dst_axi, 32);
	    //streamPush<float, FloatAxis, FloatStream>(SSD, 1, dst_axi, 32);
	}
	else
	{
		// Update value
		//tempSumSsd += input.data*input.data;
		PnnCounter += hls::fabsf(input.data) > threshold;
	}
}
