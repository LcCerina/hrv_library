#include "necstream.hpp"
//#include "ap_axi_sdata.h"
#include <hls_stream.h>
#include<stdint.h>
//#include <hls_video.h>
#include <iostream>
#include <stdio.h>

#define DATA_LEN 6

//typedef hls::stream<float> FloatStream;

void pnn_rmssd(FloatStream &src_axi, FloatStream &dst_axi);//, FloatStream &fwd_axi);

int main() {
	FloatStream in("stream_in");
	FloatStream out("stream_out");

	float test_data[DATA_LEN] = {0.064,  0.006,0.064,  0.006,0.064,  0.006, 0.16666};

	for(int i=0; i<DATA_LEN; i++)
	{
		streamPush<float, FloatAxis, FloatStream>(test_data[i], i==DATA_LEN-1, in, 32);
		//in.write(test_data[i]);
		pnn_rmssd(in, out);//, fwd);
	}
	float pnn50 = streamPop<float, FloatAxis, FloatStream>(out);
	//float rmssd = sqrtf(streamPop<float, FloatAxis, FloatStream>(out));

	std::cout << "pnn50 " << pnn50 << std::endl;
	//std::cout << "ssd " << rmssd << std::endl;
	return 0;
}
