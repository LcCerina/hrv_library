#include "necstream.hpp"
#include <hls_stream.h>
#include <stdint.h>
#include<stdio.h>

typedef ap_uint<1> data_bool;

FloatAxis f_2_s(float data, ap_uint<1> last)
{
	FloatAxis retValue;
	memsetAXIS_Data<FloatAxis>(retValue, 32);
	retValue.data = data;
	retValue.last = last;
	return retValue;
}

void rmssd_full(FloatStream &ssd_axi, FloatStream &rmssd_axi)
{
//#pragma HLS DATAFLOW
#pragma HLS INTERFACE axis port=ssd_axi
#pragma HLS INTERFACE axis port=rmssd_axi
#pragma HLS INTERFACE ap_ctrl_none port=return
	//static float threshold = 0.05;
	static float tempSum[2] = {0.0, 0.0};
#pragma HLS RESET variable=tempSum
	static float tempSumSsd = 0.0;
#pragma HLS RESET variable=tempSumSsd

	float SSD;
	float SSD_coeff;
	int count;
	int i;
	data_bool buffer_count = 0;
#pragma HLS RESET variable=buffer_count

	float ssd_input[2] = {0.0, 0.0};
#pragma HLS ARRAY_PARTITION variable=ssd_input complete dim=1


	FloatAxis input;

	// Input number of samples
	input = ssd_axi.read();
	count = (int)input.data;

	//Input coefficients
	input = ssd_axi.read();
	SSD_coeff = input.data;

	// Read stream
	read_loop: for(i=0; i<count; i++)
	{
#pragma HLS PIPELINE II=1
#pragma HLS LOOP_TRIPCOUNT min=2 max=6 avg=4
		ssd_input[buffer_count] = ssd_axi.read().data;
		tempSum[buffer_count] = ssd_input[buffer_count]*ssd_input[buffer_count];
		tempSumSsd += tempSum[buffer_count];
		++buffer_count;
	}

	// Output result
	SSD = hls::sqrtf(SSD_coeff*tempSumSsd);
	rmssd_axi.write(f_2_s(SSD, 1));
}
