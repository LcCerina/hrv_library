#include "necstream.hpp"
#include <hls_stream.h>
#include <stdint.h>
#include <stdio.h>

void StdXX(FloatStream &src_axi, FloatStream &dst_axi)
{
#pragma HLS INTERFACE axis port=dst_axi
#pragma HLS INTERFACE axis port=src_axi
#pragma HLS INTERFACE ap_ctrl_none port=return
	static float tempSum = 0.0;
#pragma HLS RESET variable=tempSum
	static float tempSumSq = 0.0;
#pragma HLS RESET variable=tempSumSq
	static float sampleCounter = 0.0;
#pragma HLS RESET variable=sampleCounter
	static float sampleFloat = 0.0;
#pragma HLS RESET variable=sampleFloat
	static float tempMean = 0.0;
#pragma HLS RESET variable=tempMean
	static float tempVar = 0.0;
#pragma HLS RESET variable=tempVar

	FloatAxis input;

	// Input
	input = streamPopFull<FloatAxis, FloatStream>(src_axi);

	// Output result
	if(input.last)
	{
		// Last sample is 1.0/samples provided by prior cores
		// Operation --> Variance = tempSumSq/Nsamples - tempSum^2/N_samples^2
		tempMean = input.data*tempSum;
		tempVar = input.data*tempSumSq - tempMean*tempMean;
		streamPush<float, FloatAxis, FloatStream>(hls::sqrtf(tempVar), 1, dst_axi, 32); //SQRT to be performed in software
	}
	else
	{
		// Update value
		tempSum += input.data;
		tempSumSq += input.data*input.data;
	}
}
