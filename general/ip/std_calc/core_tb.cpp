#include "necstream.hpp"
//#include "ap_axi_sdata.h"
#include <hls_stream.h>
#include<stdint.h>
//#include <hls_video.h>
#include <iostream>
#include <stdio.h>

#define DATA_LEN 7

//typedef hls::stream<float> FloatStream;

void StdXX(FloatStream &src_axi, FloatStream &dst_axi);//, FloatStream &fwd_axi);

int main() {
	FloatStream in("stream_in");
	FloatStream out("stream_out");
	//FloatStream fwd("stream_fwd");

	//float test_data[DATA_LEN] = {1.0,2.02,2.976,4.088,4.932,5.916,6.968};
	float test_data[DATA_LEN] = {1.02 , 0.956, 1.112, 0.844, 0.984, 1.052, 0.14285714285714285};

	for(int i=0; i<DATA_LEN; i++)
	{
		streamPush<float, FloatAxis, FloatStream>(test_data[i], i==DATA_LEN-1, in, 32);
		//in.write(test_data[i]);
		StdXX(in, out);//, fwd);
	}
	float result = streamPop<float, FloatAxis, FloatStream>(out);

	std::cout << "result " << result << std::endl;
	return 0;
}
