#include "necstream.hpp"
#include <hls_stream.h>
#include <stdint.h>
#include<stdio.h>

typedef ap_uint<2> data_count;

FloatAxis f_2_s(float data, ap_uint<1> last)
{
	FloatAxis retValue;
	memsetAXIS_Data<FloatAxis>(retValue, 32);
	retValue.data = data;
	retValue.last = last;
	return retValue;
}

void std_full(FloatStream &src_axi, FloatStream &std_axi)
{
//#pragma HLS DATAFLOW
#pragma HLS INTERFACE axis port=src_axi
#pragma HLS INTERFACE axis port=std_axi
#pragma HLS INTERFACE ap_ctrl_none port=return
	//static float threshold = 0.05;
	static float tempIn[4] = {0.0, 0.0, 0.0, 0.0};
#pragma HLS ARRAY_PARTITION variable=tempIn complete dim=1
#pragma HLS RESET variable=tempIn
	static float tempInSq[4] = {0.0, 0.0, 0.0, 0.0};
#pragma HLS ARRAY_PARTITION variable=tempInSq complete dim=1
#pragma HLS RESET variable=tempInSq
	static float tempSum = 0.0;
#pragma HLS RESET variable=tempSum
	static float tempSumSq = 0.0;
#pragma HLS RESET variable=tempSumSq

	static float tempMean = 0.0;
#pragma HLS RESET variable=tempMean
	static float tempVar = 0.0;
#pragma HLS RESET variable=tempVar

	float mul_coeff;
	int count;
	int i;
	static data_count buffer_count = 0;
#pragma HLS RESET variable=buffer_count

	float src_input[4] = {0.0, 0.0, 0.0, 0.0};
#pragma HLS ARRAY_PARTITION variable=src_input complete dim=1


	FloatAxis input;

	// Input number of samples
	input = src_axi.read();
	count = (int)input.data;

	//Input coefficients
	input = src_axi.read();
	mul_coeff = input.data;

	// Read stream
	read_loop: for(i=0; i<count; i++)
	{
#pragma HLS UNROLL
#pragma HLS PIPELINE II=1
#pragma HLS LOOP_TRIPCOUNT min=2 max=6 avg=4
		src_input[buffer_count] = src_axi.read().data;
		tempIn[buffer_count] = src_input[buffer_count];
		tempInSq[buffer_count] = src_input[buffer_count]*src_input[buffer_count];
		tempSum += tempIn[buffer_count];
		tempSumSq += tempInSq[buffer_count];
		++buffer_count;
	}

	// Output result
	tempMean = mul_coeff*tempSum;
	tempVar = mul_coeff*tempSumSq - tempMean*tempMean;
	std_axi.write(f_2_s(hls::sqrtf(tempVar), 1));
}

