#include "necstream.hpp"
//#include "ap_axi_sdata.h"
#include <hls_stream.h>
#include<stdint.h>
//#include <hls_video.h>
#include <iostream>
#include <stdio.h>

#define DATA_LEN 5

//typedef hls::stream<float> FloatStream;

void std_full(FloatStream &src_axi, FloatStream &std_axi);
FloatAxis f_2_s(float data, ap_uint<1> last);

int main() {
	FloatStream in("stream_in");
	FloatStream out("stream_out");

	float test_data[DATA_LEN] = {-0.064, 0.156, -0.268, 0.14, 0.068};


	in.write(f_2_s(5.0, 0));
	in.write(f_2_s(0.2, 0));
	for(int i=0; i<DATA_LEN; i++)
	{
		in.write(f_2_s(test_data[i], i==DATA_LEN-1));
	}
	std_full(in, out);
	float std = out.read().data;

	std::cout << "std " << std << std::endl;
	//std::cout << "ssd " << rmssd << std::endl;
	return 0;
}

