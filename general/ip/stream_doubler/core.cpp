#include "necstream.hpp"
#include <hls_stream.h>
#include <stdint.h>
#include <stdio.h>

void Axis_Doubler(FloatStream &src_axi, FloatStream &dst_axi_0, FloatStream &dst_axi_1)
{
#pragma HLS INTERFACE axis port=dst_axi_0
#pragma HLS INTERFACE axis port=dst_axi_1
#pragma HLS INTERFACE axis port=src_axi
#pragma HLS INTERFACE ap_ctrl_none port=return

	FloatAxis input;

	// Input
	input = streamPopFull<FloatAxis, FloatStream>(src_axi);

	// Output
	streamPush<float, FloatAxis, FloatStream>(input.data, input.last, dst_axi_0, 32);
	streamPush<float, FloatAxis, FloatStream>(input.data, input.last, dst_axi_1, 32);
}
