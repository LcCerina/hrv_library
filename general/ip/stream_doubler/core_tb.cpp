#include "necstream.hpp"
//#include "ap_axi_sdata.h"
#include <hls_stream.h>
#include<stdint.h>
//#include <hls_video.h>
#include <iostream>
#include <stdio.h>

#define DATA_LEN 7

//typedef hls::stream<float> FloatStream;

void Axis_Doubler(FloatStream &src_axi, FloatStream &dst_axi_0, FloatStream &dst_axi_1);

int main() {
	FloatStream in("stream_in");
	FloatStream out0("stream_out");
	FloatStream out1("stream_out");
	//FloatStream fwd("stream_fwd");

	//float test_data[DATA_LEN] = {1.0,2.02,2.976,4.088,4.932,5.916,6.968};
	float test_data[DATA_LEN] = {1.02 , 0.956, 1.112, 0.844, 0.984, 1.052, 0.14285714285714285};
	float result[2] = {0.0, 0.0};

	for(int i=0; i<DATA_LEN; i++)
	{
		streamPush<float, FloatAxis, FloatStream>(test_data[i], i==DATA_LEN-1, in, 32);
		Axis_Doubler(in, out0, out1);
		result[0] = streamPop<float, FloatAxis, FloatStream>(out0);
		result[1] = streamPop<float, FloatAxis, FloatStream>(out1);
		std::cout << "results " << result[0] << " " << result[1] << std::endl;
	}

	return 0;
}
