import numpy as np
import time
from signal import T_vals

SAMPLES = 7

def pnn_rmss(input_data, sig_len):
	SD_vals = input_data

	pnn50 = np.sum(np.abs(SD_vals) > 0.05) / sig_len
	rmssd = np.sqrt(np.sum(SD_vals*SD_vals) / sig_len)
	return pnn50, rmssd

def std1(input_data, sig_len):
	tempSum = 0
	for i in range(sig_len):
		tempSum = tempSum + input_data[i]
	tempMean = tempSum / sig_len
	sq_diff_sum = 0
	for i in range(sig_len):
		tempDiff = input_data[i] - tempMean
		sq_diff_sum = sq_diff_sum + tempDiff*tempDiff
	tempVar = sq_diff_sum / sig_len
	return np.sqrt(tempVar)

def std2(input_data, sig_len):
	tempSum = np.float32(0.0)
	tempSqSum = np.float32(0.0)
	for i in range(sig_len):
		tempSum = tempSum + input_data[i]
		tempSqSum = tempSqSum + input_data[i]*input_data[i]
	tempMean = tempSum / (np.float32(sig_len))
	tempVar = tempSqSum / (np.float32(sig_len)) - tempMean*tempMean
	return np.sqrt(tempVar)	

def std_true(input_data):
	return np.std(input_data)	
	
def SDx_process(sdnn, sdsd):

	SD1_temp = 0.5 * sdsd * sdsd
	SD1 = np.sqrt(SD1_temp)
	SD2 = np.sqrt(2*sdnn*sdnn - SD1_temp)
	SD_ratio = SD1 / SD2

	return SD1, SD2, SD_ratio

T_vals = np.array(T_vals)
T_vals = T_vals[0:SAMPLES]
tStart = time.perf_counter()
RR_vals = np.diff(T_vals)
SD_vals = np.diff(RR_vals)

pnn, rmss = pnn_rmss(SD_vals, SAMPLES-2)
#print("PNN50 = {}".format(pnn))
print("RMSSD = {}".format(rmss))
#print("Time taken: {}".format(tEnd-tStart))

#tStart = time.perf_counter()
sdnn = std2(RR_vals, SAMPLES-1)
sdsd = std2(SD_vals, SAMPLES-2)
#tEnd = time.perf_counter()

#print("SDNN  = {}".format(sdnn))
#print("SDSD  = {}".format(sdnn2))
#print("Time taken: {}".format(tEnd-tStart))

#tStart = time.perf_counter()
sd1, sd2, sd_ratio = SDx_process(sdnn, sdsd)
tEnd = time.perf_counter()

#print("SD1 = {}".format(sd1))
#print("SD2 = {}".format(sd2))
#print("SD ratio = {}".format(sd_ratio))
print("{}   {:.6E}".format(SAMPLES, tEnd-tStart))
