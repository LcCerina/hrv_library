#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>
#include "signal.h"

#define SAMPLES 512
#define VERBOSE 1
#define VERBOSEFOR 0

using namespace std;
using namespace std::chrono;

void RR_gen(float *input, float *output, int samples)
{
    int i;
    for(i=1; i<samples; i++)
    {
        output[i-1] = input[i] - input[i-1];
    }
}

void RMSSD_PNN(float *input, float *output, int samples)
{
    float threshold = 0.05;
    int NN50 = 0;
    float tempSumSSD = 0.0;

    for(int i=0; i<samples; i++)
    {
        //printf("nn %d\n", fabsf(input[i])> threshold);
        NN50 += fabsf(input[i]) > threshold;
        tempSumSSD += input[i]*input[i];
    }
    output[0] = NN50 /(float)samples;
    output[1] = sqrtf(tempSumSSD / (float)samples);
}

void SDxx(float *input, float *output, int samples)
{
    float tempSum = 0.0;
    float tempSumSq = 0.0;
    float tempMean;
    float tempVar;

    for(int i=0; i<samples; i++)
    {
        tempSum += input[i];
        tempSumSq += input[i]*input[i];
    }
    //std::cout << "tempsum " << tempSum << std::endl;
    //std::cout << "tempsumsq " << tempSumSq << std::endl;
    tempMean = tempSum / samples;
    tempVar = tempSumSq / samples - tempMean*tempMean;
    *output = sqrtf(tempVar);
}

void SD_process(float *sdnn, float *sdsd, float *output)
{
    float tempSD1 = 0.5 * (*sdsd) * (*sdsd);
    output[0] = sqrtf(tempSD1);
    output[1] = sqrtf(2*(*sdnn)*(*sdnn) - tempSD1);
    output[2] = output[0]/output[1]; 
}

int main()
{
        // Timer variables
    high_resolution_clock::time_point tStart, tEnd;
    tStart = high_resolution_clock::now();
    tEnd = high_resolution_clock::now();    
    uint64_t duration;
    duration = duration_cast<nanoseconds>(tEnd - tStart).count();
    std::cout << "Calib duration: " << duration << std::endl;
    uint64_t total_duration;

    std::cout << "samples = " <<  SAMPLES << std::endl;
    std::cout << std::setprecision(12);
    float test_data[SAMPLES];

    float RR[SAMPLES-1];
    float SSD[SAMPLES-2];

    float SDNN = {0.0};
    float SDSD = {0.0};
    float PNN50_RMSSD[2] = {0.0,0.0};
    float SD_values[3] = {0.0,0.0,0.0};

    int i;
    for(i = 0; i<SAMPLES; i++)
    {
        test_data[i] = signal[i+SAMPLES/2];
    }
        
    tStart = high_resolution_clock::now();
    // Generate RR
    RR_gen(test_data, RR, SAMPLES);
    for(i=0; i<SAMPLES-1 && VERBOSEFOR;i++)
    {
        std::cout << "RR[" << i << "] = "<<  RR[i] << std::endl;
    }
    // Generate SSD
    RR_gen(RR, SSD, SAMPLES-1);
    for(i=0; i<SAMPLES-2 && VERBOSEFOR;i++)
    {
        std::cout << "SSD[" << i << "] = "<<  SSD[i] << std::endl;
    }

    // Calculate PNN50 and RMSSD
    RMSSD_PNN(SSD, PNN50_RMSSD, SAMPLES-2);
    if(VERBOSE)
    {
        std::cout << "PNN50 = " << PNN50_RMSSD[0] << std::endl;
        std::cout << "RMSSD = " << PNN50_RMSSD[1] << std::endl;
    }

    // Calculate SDNN and SDSD
    SDxx(RR, &SDNN, SAMPLES-1);
    SDxx(SSD, &SDSD, SAMPLES-2);
    if(VERBOSE)
    {
        std::cout << "SDNN = " << SDNN << std::endl;
        std::cout << "SDSD = " << SDSD << std::endl;
    }

    // Calculate SD1, SD2 and SD1/SD2
    SD_process(&SDNN, &SDSD, SD_values);
    if(VERBOSE)
    {
        std::cout << "SD1 = " << SD_values[0] << std::endl;
        std::cout << "SD1 = " << SD_values[1] << std::endl;
        std::cout << "SD1/SD2 = " << SD_values[2] << std::endl;
    }    

    tEnd = high_resolution_clock::now();
    total_duration = duration_cast<nanoseconds>(tEnd - tStart).count() - duration;
    cout << std::scientific;
    std::cout << "Duration: " << total_duration << " " << total_duration/1e+09 << std::endl;
}
