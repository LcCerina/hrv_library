/******************************************************************************
 *
 * Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Use of the Software is limited solely to applications:
 * (a) running on a Xilinx device, or
 * (b) that interact with a Xilinx device through a bus or interconnect.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Except as contained in this notice, the name of the Xilinx shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Xilinx.
 *
 ******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>
#include "platform.h"
#include "xil_printf.h"
#include "xtime_l.h"
#include "stdint.h"
#include "xaxidma.h"
#include "xgpio.h"
#include "sleep.h"
#include <math.h>
#include "signal.h"

#define SAMPLES 512
#define TESTS 1
#define VERBOSE 0
#define VERBOSEFOR 0
#define CORES 1

XTime outTime(XTime tStart, XTime tEnd, XTime tCal)
{
	return 2*(tEnd-tStart-tCal);
}

int init_dma(XAxiDma *axiDmaPtr, int deviceId) {
	XAxiDma_Config *CfgPtr;
	int status;
	// Get pointer to DMA configuration
	CfgPtr = XAxiDma_LookupConfig(deviceId);
	if (!CfgPtr) {
		xil_printf("Error looking for AXI DMA config\n\r");
		return XST_FAILURE;
	}
	// Initialize the DMA handle
	status = XAxiDma_CfgInitialize(axiDmaPtr, CfgPtr);
	if (status != XST_SUCCESS) {
		xil_printf("Error initializing DMA\n\r");
		return XST_FAILURE;
	}
	//check for scatter gather mode - this example must have simple mode only
	if (XAxiDma_HasSg(axiDmaPtr)) {
		xil_printf("Error DMA configured in SG mode\n\r");
		return XST_FAILURE;
	}
	//disable the interrupts
	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);

	return XST_SUCCESS;
}

int main()
{
	init_platform();
	Xil_ICacheDisable();
	Xil_DCacheDisable();

	// Define AXI devices
	int axi_dma_devices[2] = {XPAR_AXI_DMA_SHOT_0_DEVICE_ID, XPAR_AXI_DMA_SHOT_1_DEVICE_ID};//, XPAR_AXI_DMA_SHOT_2_DEVICE_ID};
	int axi_gpio_devices[2] = {XPAR_HRV_CORE1_IP_CTRL_DEVICE_ID, XPAR_HRV_CORE2_IP_CTRL_DEVICE_ID};//, XPAR_HRV_CORE3_IP_CTRL_DEVICE_ID};

	XAxiDma axi_dma[3];
	XGpio axi_gpio[3];
	int ok;
	int i, j,idx;

	// Allocate input data
	float test_data[SAMPLES*CORES + 3*CORES];

	// Allocate results
	float results[3*CORES];
	float Pnn50[CORES], RMSSD[CORES], SDRR[CORES], SDSD[CORES], SD1[CORES], SD2[CORES], SD_ratio[CORES];
	float tempSD1;

	// Timer variables
	XTime tStart, tEnd, tCal;
	XTime calcCycles = 0;
	// Calibrate Timer
	XTime_GetTime(&tStart);
	XTime_GetTime(&tEnd);
	tCal = tEnd - tStart;

	// Prefill input array
	for(i=0; i < CORES; i++)
	{
		// Transfer infos
		test_data[SAMPLES*i + 3*i] = (float)(SAMPLES-1.0);
		test_data[SAMPLES*i + 3*i +1] = 1.0/(float)(SAMPLES-1.0);
		test_data[SAMPLES*i + 3*i +2] = 1.0/(float)(SAMPLES-2.0);
		// ECG data
		for(j=3*(i+1); j < (SAMPLES + 3*(i+1)); j++)
		{
			test_data[j + SAMPLES*i] = signal[j -3*(i+1) + (SAMPLES*i)/2];
		}
	}

	// Execute multiple tests
	for(int reps=0; reps<TESTS; reps++)
	{
		// Init DMAs
		for(i=0; i < CORES; i++)
		{
			// Init
			ok = init_dma(&axi_dma[i], axi_dma_devices[i]);
			if(ok != XST_SUCCESS)
			{
				printf("No init dma%d\n\r", i);
				exit(1);
			}
			// Reset
			XAxiDma_Reset(&axi_dma[i]);
			while(!XAxiDma_ResetIsDone(&axi_dma[i]));
			if(VERBOSE)
				printf("Dma%d reset done\n\r", i);
		}

		// Init GPIOs
		for(i=0; i <CORES; i++)
		{
			// Init
			ok = XGpio_Initialize(&axi_gpio[i], axi_gpio_devices[i]);
			if(ok != XST_SUCCESS)
			{
				printf("No init GPIO%d\n\r", i);
				exit(1);
			}
			// Core reset
			XGpio_WriteReg(axi_gpio[i].BaseAddress, 0, 0x00000001);
			XGpio_WriteReg(axi_gpio[i].BaseAddress, 0, 0x00000000);
		}

		// Start timer
		XTime_GetTime(&tStart);

		// Data transfer
		for(i=0; i<CORES; i++)
		{
			ok = XAxiDma_SimpleTransfer(&axi_dma[i], (unsigned int)(&test_data[SAMPLES*i + 3*i]), (SAMPLES+3)*sizeof(float), XAXIDMA_DMA_TO_DEVICE);
			if(ok != XST_SUCCESS)
			{
				print("Error in data transfer\n\r");
				printf("%d\n\r", ok);
			}
			//while (XAxiDma_Busy(&axi_dma[i], XAXIDMA_DMA_TO_DEVICE));
		}
		//XTime_GetTime(&tEnd);

		// Get data back from the core
		//XTime_GetTime(&tStart);
		for(i=0; i<CORES; i++)
		{
			ok = XAxiDma_SimpleTransfer(&axi_dma[i], (unsigned int)(&results[3*i]), (3)*sizeof(float), XAXIDMA_DEVICE_TO_DMA);
			if(ok != XST_SUCCESS)
			{
				print("Error in data transfer\n\r");
				printf("%d\n\r", ok);
			}
			// Wait the last core
			if(i==(CORES-1))
			{
				while (XAxiDma_Busy(&axi_dma[i], XAXIDMA_DEVICE_TO_DMA));
			}
		}
		//XTime_GetTime(&tEnd);

		// Software execution
		//XTime_GetTime(&tStart);
		for(i=0; i<CORES; i++)
		{
			idx = 3*i;
			SDRR[i] = results[idx];
			Pnn50[i] = results[idx + 1];
			RMSSD[i] = results[idx + 2];
			//XTime_GetTime(&tStart);
			SDSD[i] = RMSSD[i];
			tempSD1 = 0.5*SDSD[i]*SDSD[i];
			SD1[i] = sqrtf(tempSD1);
			SD2[i] = sqrtf(2.0*SDRR[i]*SDRR[i]- tempSD1);
			SD_ratio[i] = SD1[i]/SD2[i];
		}

		// End Timer
		XTime_GetTime(&tEnd);
		calcCycles = outTime(tStart, tEnd, tCal);
		printf("%d\t%e\t%llu\n", SAMPLES*CORES,calcCycles/650000000.0, calcCycles);
	}

	if(VERBOSE)
	{
		for(i=0; i<CORES; i++)
		{
			printf("\n\r");
			printf("RR std = %.12f \n\r", SDRR[i]);
			printf("SD std = %.12f \n\r", SDSD[i]);
			printf("Pnn50 = %.12f \n\r", Pnn50[i]);
			printf("RMSSD = %.12f \n\r", RMSSD[i]);
			printf("SD1 = %.12f \n\r", SD1[i]);
			printf("SD2 = %.12f \n\r", SD2[i]);
			printf("SD_ratio = %.12f \n\r", SD_ratio[i]);
		}
	}

	cleanup_platform();
	return 0;
}
