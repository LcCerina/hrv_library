/******************************************************************************
 *
 * Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Use of the Software is limited solely to applications:
 * (a) running on a Xilinx device, or
 * (b) that interact with a Xilinx device through a bus or interconnect.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Except as contained in this notice, the name of the Xilinx shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Xilinx.
 *
 ******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>
#include "platform.h"
#include "xil_printf.h"
#include "xtime_l.h"
#include "stdint.h"
#include "xaxidma.h"
#include "xgpio.h"
#include "sleep.h"
#include <math.h>
#include "signal.h"

#define SAMPLES 50000
#define TESTS 10
#define VERBOSE 0
#define VERBOSEFOR 0

XTime outTime(XTime tStart, XTime tEnd, XTime tCal)
{
	return 2*(tEnd-tStart-tCal);
}

int init_dma(XAxiDma *axiDmaPtr, int deviceId) {
	XAxiDma_Config *CfgPtr;
	int status;
	// Get pointer to DMA configuration
	CfgPtr = XAxiDma_LookupConfig(deviceId);
	if (!CfgPtr) {
		xil_printf("Error looking for AXI DMA config\n\r");
		return XST_FAILURE;
	}
	// Initialize the DMA handle
	status = XAxiDma_CfgInitialize(axiDmaPtr, CfgPtr);
	if (status != XST_SUCCESS) {
		xil_printf("Error initializing DMA\n\r");
		return XST_FAILURE;
	}
	//check for scatter gather mode - this example must have simple mode only
	if (XAxiDma_HasSg(axiDmaPtr)) {
		xil_printf("Error DMA configured in SG mode\n\r");
		return XST_FAILURE;
	}
	//disable the interrupts
	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);

	return XST_SUCCESS;
}

int main()
{
	init_platform();
	Xil_ICacheDisable();
	Xil_DCacheDisable();

	XAxiDma Axis_0, Axis_1;
	XAxiDma Axis_std;
	XAxiDma Axis_rmssd;
	XAxiDma Axis_pnn;
	XGpio Gpio0, Gpio1;
	int ok;
	int i;

	//print("Hello World\n\r");
	//printf("Test with %d samples\n\r", SAMPLES);

	float numSamples[1] = {(float)SAMPLES};
	float RRCoeff[1] = {(float)1.0/(SAMPLES-1)};

	float test_data[SAMPLES*2 + 8];
	//float test_mini[] = {6.0, 0.15, 1.0, 2.02, 2.976, 4.088, 4.932, 5.916, 6.968};
	float output_single[SAMPLES+10];
	float output_full[SAMPLES+10];
	float results[6];


	// Results
	float Pnn50[2], RMSSD[2], SDRR[2], SDSD[2], SD1[2], SD2[2], SD_ratio[2];
	float tempSD1;
	// Prefill input array
	test_data[0] = (float)(SAMPLES-1.0);
	test_data[1] = 1.0/(float)(SAMPLES-1.0);
	test_data[2] = 1.0/(float)(SAMPLES-2.0);
	for(i=3; i < (SAMPLES+3); i++)
	{
		test_data[i] = signal[i-3];
		//output_single[i-3] = 0.0;
		//output_full[i-3] = 0.0;
	}
	test_data[SAMPLES+3] = (float)(SAMPLES-1.0);
	test_data[SAMPLES+4] = 1.0/(float)(SAMPLES-1.0);
	test_data[SAMPLES+5] = 1.0/(float)(SAMPLES-2.0);
	for(i=6; i < (SAMPLES+6); i++)
	{
		test_data[i+SAMPLES] = signal[i-6+SAMPLES/2];
		//output_single[i-3] = 0.0;
		//output_full[i-3] = 0.0;
	}
	// Timer variables
	XTime tStart, tEnd, tCal;
	XTime calcCycles = 0;
	// Calibrate Timer
	XTime_GetTime(&tStart);
	XTime_GetTime(&tEnd);
	tCal = tEnd - tStart;

	for(int reps=0; reps<TESTS; reps++)
	{
		// Init DMAs
		ok = init_dma(&Axis_0, XPAR_AXI_DMA_SHOT_0_DEVICE_ID);
		if(ok != XST_SUCCESS)
		{
			print("No init dma1\n\r");
			exit(1);
		}
		ok = init_dma(&Axis_1, XPAR_AXI_DMA_SHOT_1_DEVICE_ID);
		if(ok != XST_SUCCESS)
		{
			print("No init dma1\n\r");
			exit(1);
		}
		// DMA0 reset
		XAxiDma_Reset(&Axis_0);
		while(!XAxiDma_ResetIsDone(&Axis_0));
		//print("reset dma 0 done\n\r");
		XAxiDma_Reset(&Axis_1);
		while(!XAxiDma_ResetIsDone(&Axis_1));
		//print("reset dma 1 done\n\r");


		// Init GPIOs
		ok = XGpio_Initialize(&Gpio0, XPAR_HRV_CORE1_IP_CTRL_DEVICE_ID);
		if(ok != XST_SUCCESS)
		{
			print("No init GPIO0\n\r");
			exit(1);
		}
		ok = XGpio_Initialize(&Gpio1, XPAR_HRV_CORE2_IP_CTRL_DEVICE_ID);
		if(ok != XST_SUCCESS)
		{
			print("No init GPIO1\n\r");
			exit(1);
		}

		// Core reset
		//XGpio_DiscreteWrite(&Gpio0, 1, 0x00000001);
		//XGpio_DiscreteWrite(&Gpio0, 1, 0x00000000);
		//XGpio_WriteReg(Gpio0.BaseAddress, 0, 0x00000001);
		//XGpio_WriteReg(Gpio0.BaseAddress, 0, 0x00000000);
		XGpio_WriteReg(Gpio1.BaseAddress, 0, 0x00000001);
		XGpio_WriteReg(Gpio1.BaseAddress, 0, 0x00000000);
		XGpio_WriteReg(Gpio0.BaseAddress, 0, 0x00000001);
		XGpio_WriteReg(Gpio0.BaseAddress, 0, 0x00000000);

		// Start timer
		XTime_GetTime(&tStart);
		// Data transfer
		ok = XAxiDma_SimpleTransfer(&Axis_0, (unsigned int)test_data, (SAMPLES+3)*sizeof(float), XAXIDMA_DMA_TO_DEVICE);
		if(ok != XST_SUCCESS)
		{
			print("Error in data transfer\n\r");
			printf("%d\n\r", ok);
			//exit(1);
		}
		//while (XAxiDma_Busy(&Axis_0, XAXIDMA_DMA_TO_DEVICE));
		ok = XAxiDma_SimpleTransfer(&Axis_1, (unsigned int)(&test_data[SAMPLES+3]), (SAMPLES+3)*sizeof(float), XAXIDMA_DMA_TO_DEVICE);
		if(ok != XST_SUCCESS)
		{
			print("Error in data transfer\n\r");
			printf("%d\n\r", ok);
			//exit(1);
		}
		//while (XAxiDma_Busy(&Axis_0, XAXIDMA_DMA_TO_DEVICE));
		//XTime_GetTime(&tEnd);
		//print("Transfer to device done\n\r");
		// Get data back from the core
		ok = XAxiDma_SimpleTransfer(&Axis_0, (unsigned int)(results), (3)*sizeof(float), XAXIDMA_DEVICE_TO_DMA);
		if(ok != XST_SUCCESS)
		{
			print("Error in data transfer\n\r");
			printf("%d\n\r", ok);
			//exit(1);
		}
		//sleep(1);
		//while (XAxiDma_Busy(&Axis_0, XAXIDMA_DEVICE_TO_DMA));
		ok = XAxiDma_SimpleTransfer(&Axis_1, (unsigned int)(&results[3]), (3)*sizeof(float), XAXIDMA_DEVICE_TO_DMA);
		if(ok != XST_SUCCESS)
		{
			print("Error in data transfer\n\r");
			printf("%d\n\r", ok);
			//exit(1);
		}
		//sleep(1);
		while (XAxiDma_Busy(&Axis_1, XAXIDMA_DEVICE_TO_DMA));
		// Get data back from the core
		/*ok = XAxiDma_SimpleTransfer(&Axis_pnn, (unsigned int)(&Pnn50), (1)*sizeof(float), XAXIDMA_DEVICE_TO_DMA);
	if(ok != XST_SUCCESS)
	{
		print("Error in data transfer\n\r");
		printf("%d\n\r", ok);
		//exit(1);
	}
	//sleep(1);
	//while (XAxiDma_Busy(&Axis1, XAXIDMA_DEVICE_TO_DMA));

	// Get data back from the core
	ok = XAxiDma_SimpleTransfer(&Axis_rmssd, (unsigned int)(&RMSSD), (1)*sizeof(float), XAXIDMA_DEVICE_TO_DMA);
	if(ok != XST_SUCCESS)
	{
		print("Error in data transfer\n\r");
		printf("%d\n\r", ok);
		//exit(1);
	}
	//sleep(1);
	while (XAxiDma_Busy(&Axis_rmssd, XAXIDMA_DEVICE_TO_DMA));*/

		// Timer for software overhead
		SDRR[0] = results[0];
		Pnn50[0] = results[1];
		RMSSD[0] = results[2];
		//XTime_GetTime(&tStart);
		SDSD[0] = RMSSD[0];
		tempSD1 = 0.5*SDSD[0]*SDSD[0];
		SD1[0] = sqrtf(tempSD1);
		SD2[0] = sqrtf(2.0*SDRR[0]*SDRR[0]- tempSD1);
		SD_ratio[0] = SD1[0]/SD2[0];

		SDRR[1] = results[3];
		Pnn50[1] = results[4];
		RMSSD[1] = results[5];
		//XTime_GetTime(&tStart);
		SDSD[1] = RMSSD[1];
		tempSD1 = 0.5*SDSD[1]*SDSD[1];
		SD1[1] = sqrtf(tempSD1);
		SD2[1] = sqrtf(2.0*SDRR[1]*SDRR[1]- tempSD1);
		SD_ratio[1] = SD1[1]/SD2[1];

		// End Timer
		XTime_GetTime(&tEnd);
		calcCycles = outTime(tStart, tEnd, tCal);
		printf("%d\t%e\t%llu\n", SAMPLES*2,calcCycles/650000000.0, calcCycles);
	}
	//printf("transfer done.\n\r");
	if(VERBOSE)
	{
		printf("RR std = %.12f \n\r", SDRR[0]);
		printf("SD std = %.12f \n\r", SDSD[0]);
		printf("Pnn50 = %.12f \n\r", Pnn50[0]);
		printf("RMSSD = %.12f \n\r", RMSSD[0]);

		printf("SD1 = %.12f \n\r", SD1[0]);
		printf("SD2 = %.12f \n\r", SD2[0]);
		printf("SD_ratio = %.12f \n\r", SD_ratio[0]);
	}
	if(VERBOSE)
	{
		printf("\n\r");
		printf("RR std = %.12f \n\r", SDRR[1]);
		printf("SD std = %.12f \n\r", SDSD[1]);
		printf("Pnn50 = %.12f \n\r", Pnn50[1]);
		printf("RMSSD = %.12f \n\r", RMSSD[1]);

		printf("SD1 = %.12f \n\r", SD1[1]);
		printf("SD2 = %.12f \n\r", SD2[1]);
		printf("SD_ratio = %.12f \n\r", SD_ratio[1]);
	}

	cleanup_platform();
	return 0;
}
;
